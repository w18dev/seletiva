<?php include 'header.php'; ?>
	<?php
        $post = get_post('4'); 
        $postId = $post->ID;
        $background_home = get_field('background_home', $postId);
        $titulo_home = get_field('titulo_home', $postId);
        $conteudo_home = get_field('conteudo_home', $postId);
        $botao_banner = get_field('botao_banner', $postId);
        $link_botao = get_field('link_botao', $postId);
        $titulo_info = get_field('titulo_info', $postId);
        $conteudo_info = get_field('conteudo_info', $postId);
        $banner_info = get_field('banner_info', $postId);
        $banner_info2 = get_field('banner_info_pro', $postId);
    ?>
	<div class="row index index-bkg" style="background-image: url(<?php echo $background_home; ?>)">
		<div class="container center section">
			<div class="row col-lg-12 col-sm-12 col-md-5 text-left section-container">
				<div class="section-title montserrat_bold col-lg-5  col-sm-9">
					<h1><?php echo $titulo_home; ?></h1>
				</div>
				<div class="section-content col-lg-4 col-sm-7 roboto_light">
					<p><?php echo $conteudo_home; ?></p>
				</div>
			</div>
		</div>
	</div>
	
	<?php include 'nossos-servicos.php'; ?>
	
	<div class="row aside aside-bkg1" style="background-image: url(<?php echo $banner_info; ?>)">
		<div class="col-lg-12 aside-container">
			<div class="container center">
				<div class="aside-title col-lg-4 montserrat_bold">
					<h1><?php echo $titulo_info; ?></h1>
				</div>
				<div class="aside-content col-lg-9 opensans_light">
					<p><?php echo $conteudo_info; ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="row aside aside-bkg2" style="background-image: url(<?php echo $banner_info2; ?>)">
		<div class="col-lg-12">
			<div class="container center aside-produtos">
				<?php
					$post = get_post('4'); 
					// check if the repeater field has rows of data
					 	// loop through the rows of data
					while ( have_rows('produtos_carousel') ) : the_row();
						$texto = get_sub_field('produto_carousel_info', $postId);
					        // display a sub field value 
				?>
					<div class="aside-produtos--list opensans_light col-lg-3 left">
						<nav>
							<ul>
								<li><i class="fa fa-circle"></i><?php echo $texto; ?></li>
							</ul>
						</nav>
					</div>
				<?php endwhile;?>
			</div>
		</div>
		<div class="container center col-lg-12">
			<div class="aside-produtos--plus col-lg-6 montserrat_bold text-center">
				<a href="<?php bloginfo('template_directory')?>/contato">QUER SABER MAIS? FALE COM A GENTE</a>
			</div>	
		</div>
	</div>

	
<?php include 'meio-ambiente.php'; ?>

<?php include 'fale-conosco.php'; ?>

<?php include 'contato.php'; ?>
	
<?php include 'footer.php'; ?>