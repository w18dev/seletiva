<?php 
/*
##############################
	GETs
##############################
*/
	/*
	================================
		NAVIGATION
	================================
	*/
		function getNavigation(){
			include_once 'navigation.php';
		}
	/*
	================================
		//NAVIGATION
	================================
	*/
	/*
	================================
		MENU
	================================
	*/
		function getMenu($p_args){
			switch ($p_args["tipo"]) {
				case 'list-inline':
					$menuClass 		= "menu menu-list--inline";
					if($p_args["menu_class"] != null){
						$menuClass = $menuClass." ".$p_args["menu_class"];
					}
					$args = array(
							'menu' 			=> $p_args["nome"],
							'menu_class'	=> $menuClass,
							'container'		=> "ul"
						);
					wp_nav_menu($args);
					break;
				case 'list-block':
					$menuClass 		= "menu menu-list--block";
					if($p_args["menu_class"] != null){
						$menuClass = $menuClass." ".$p_args["menu_class"];
					}
					$args = array(
							'menu' 			=> $p_args["nome"],
							'menu_class'	=> $menuClass,
							'container'		=> "ul"
						);
					wp_nav_menu($args);
					break;
			}
		}
	/*
	================================
		//MENU
	================================
	*/
	/*
	================================
		RESUME CONTENT
	================================
	*/
		function getResumeContent($content){
			if(strlen($content) > 250){
				$content  = strip_tags($content);
	            $content  = trim($content);
				$content = substr($content, 0, 250);
				$pos = strrpos($content, " ");
				$resume = substr($content, 0, $pos)."...";
			}else{
				$resume = $content;
			}
			return $resume;
		}
	/*
	================================
		//RESUME CONTENT
	================================
	*/
	/*
	================================
		RESUME TITLE
	================================
	*/
		function getResumeTitle($content){
			if(strlen($content)>= 55){
				$resume = substr($content, 0, 40)."...";				
			}else{
				$resume = $content;
			}
			return $resume;
		}
	/*
	================================
		//RESUME TITLE
	================================
	*/
	/*
	================================
		COMMENTS FACEBOOK
	================================
	*/
		function getFacebookCommentsCounter($url){
			$json = json_decode(file_get_contents('https://graph.facebook.com/?ids='.$url));
			return ($json->$url->share->comment_count) ? $json->$url->share->comment_count : 0;
		}
	/*
	================================
		//COMMENTS FACEBOOK
	================================
	*/
	/*
	================================
		LIKES FACEBOOK
	================================
	*/
		function getFacebookLikesCounter($url){
			$json = json_decode(file_get_contents('https://graph.facebook.com/?ids='.$url));
			return ($json->$url->share->share_count) ? $json->$url->share->share_count : 0;
		}
	/*
	================================
		//LIKES FACEBOOK
	================================
	*/
/*
##############################
	//GETs
##############################
*/
/*
#############################
	HELPERS FUNCTIONS
#############################
*/
	/*
	==============================
		REMOVE PARAGRAPH
	==============================
	*/
		function removeParagraph($data){
			$data  = str_replace("<p>", "", $data);
            $data  = str_replace("</p>", "", $data);
            $data  = trim($data);
            return $data;
		}
	/*
	==============================
		//REMOVE PARAGRAPH
	==============================
	*/
	/*
	==============================
		SHOW RESULT
	==============================
	*/
		function showResult($data){
			echo "<pre>";
			print_r($data);
			echo "</pre>";
		}
	/*
	==============================
		//SHOW RESULT
	==============================
	*/
/*
#############################
	//HELPERS FUNCTIONS
#############################
*/
/*
#############################
	SUPPORT WP
#############################
*/
	/*
	---------------------------
		MENUS
	---------------------------
	*/
		add_theme_support("menus");
	/*
	---------------------------
		//MENUS
	---------------------------
	*/
/*
#############################
	//SUPPORT WP
#############################
*/

	

	/*
		#######################
	      NOSSOS EQUIPAMENTOS
	    #######################
	*/
	
		add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
		add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');
		

		function load_posts_by_ajax_callback() {
	    check_ajax_referer('load_more_posts', 'security');
	    $paged = $_POST['page'];
	    $args = array(
	        'post_type' => 'nossos_equipamentos',
	        'post_status' => 'publish',
	        'posts_per_page' => '6',
	        'order' => 'ASC',
	        'paged' => $paged,
	    );
	   		 $my_posts = new WP_Query( $args );
		    if ( $my_posts->have_posts() ) :
		        ?>
		        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 
		        	$post 		= get_post();        
					$postId 	= $post->ID; 
					$imagem 	= get_field('imagem_do_post', $postId); 	
		        ?>
		            <div class="estrutura-posts col-lg-4 col-xs-12 col-sm-6 container left" data-est="<?php echo $postId; ?>">
						<div class="posts-img">
							<img src="<?php echo $imagem; ?>" id="estrutura-posts--img--<?php echo $postId?>">
							<div class="posts-content col-lg-12 col-xs-12">
								<div class="posts-content--text montserrat_bold text-center col-lg-8 center">
									<p id="estrutura-posts--text--<?php echo $postId; ?>"><?php the_title(); ?></p>
								</div>	
							</div>
						</div>
					</div>
		        <?php endwhile ?>
		        <?php
		    endif;
		 
		    wp_die();
		}

	/*
		########################
	     //NOSSOS EQUIPAMENTOS
	    ########################
	*/


	/*
		##################
	      NOSSOS PRODUTOS
	    ##################
	*/
	
		add_action('wp_ajax_load_posts_by_ajax_produtos', 'load_posts_by_ajax_callback_produtos');
		add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback_produtos');
		

		function load_posts_by_ajax_callback_produtos() {
	    check_ajax_referer('load_more_posts', 'security');
	    $paged = $_POST['page'];
	    $args = array(
	        'post_type' => 'nossos_produtos',
	        'post_status' => 'publish',
	        'posts_per_page' => '6',
	        'order' => 'ASC',
	        'paged' => $paged,
	    );
	   		 $my_posts = new WP_Query( $args );
		    if ( $my_posts->have_posts() ) :
		        ?>
		        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 
		        	$post 		= get_post();        
					$postId_prod 	= $post->ID; 
					$imagem_prod 	= get_field('imagem_do_post', $postId_prod); 	
		        ?>
		        <div class="produtos-posts col-lg-4 col-xs-12 col-sm-6 container left" data-post="<?php echo $postId_prod; ?>">
					<div class="produtos-posts--img">
						<img src="<?php echo $imagem_prod; ?>" id="produtos-posts--img--<?php echo $postId_prod; ?>">
						<div class="produtos-posts--content col-lg-12 col-xs-12">
							<div class="produtos-posts--text montserrat_bold text-center col-lg-7 center">
								<p id="produtos-posts--text--<?php echo $postId_prod; ?>"><?php the_title(); ?></p>
							</div>	
						</div>
					</div>
				</div>
		        <?php endwhile ?>
		        <?php
		    endif;
		 
		    wp_die();
		}

	/*
		###################
	     //NOSSOS PRODUTOS
	    ###################
	*/


?>