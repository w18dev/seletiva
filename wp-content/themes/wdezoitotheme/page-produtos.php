<?php include 'header-pages.php'; ?>

	<div class="row col-lg-12 col-xs-12 col-sm-12 produtos">
		<div class="container center">
			<?php
				$post = get_post('15'); 
				$postId = $post->ID;
				$titulo = get_field('titulo_produtos', $postId);
				$subtitulo = get_field('subtitulo_produtos', $postId);
				$conteudo = get_field('conteudo_produtos', $postId);

			?>
			<div class="container mobile-space">
				<div class="produtos-title montserrat_bold">
					<h1><?php echo $titulo; ?></h1>
				</div>
				<div class="produtos-subtitle montserrat_bold">
					<h2><?php echo $subtitulo; ?></h2>
				</div>
				<div class="produtos-content  gotham_regular">
					<?php echo $conteudo; ?>
				</div>
			</div>
			<div class="col-lg-12 col-xs-12 col-sm-12 produtos-align">
				<div class="entry-content">
				    <?php 
				    $args = array(
				        'post_type' => 'nossos_produtos',
				        'post_status' => 'publish',
				        'order' => 'ASC',
				    );
				    $my_posts = new WP_Query( $args );
				    if ( $my_posts->have_posts() ) : 
				    ?>
				        <div class="my-posts">
				            <?php while ( $my_posts->have_posts() ) : $my_posts->the_post();
				            	$post 		= get_post();        
								$postId 	= $post->ID; 
								$imagem 	= get_field('imagem_do_post', $postId); 	
				             ?>
				                <div class="produtos-posts col-lg-4 col-xs-12 col-sm-6 container left" data-post="<?php echo $postId; ?>">
									<div class="produtos-posts--img">
										<img src="<?php echo $imagem; ?>" id="produtos-posts--img--<?php echo $postId; ?>">
										<div class="produtos-posts--content col-lg-12 col-xs-12">
											<div class="produtos-posts--text montserrat_bold text-center col-lg-7 center">
												<p id="produtos-posts--text--<?php echo $postId; ?>"><?php the_title(); ?></p>
											</div>	
										</div>
									</div>
								</div>
				            <?php endwhile; endif; ?>
				        </div> 
				</div>
			</div>
			<!-- <div class="row col-lg-12 col-xs-12">
				<div class="row produtos-plus">
					<div class="produtos-plus--link text-center montserrat_bold" id="load-default">
						<a href="#" id="produtos-plus">Carregar mais...</a>
					</div>
					<div id="page-loading" style="display: none;" class="text-center">
						<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
					</div>
				</div>
			</div> -->
		</div>
	</div>



<?php include 'fale-conosco.php'; ?>

<?php include 'contato.php'; ?>

<?php include 'footer.php'; ?>

<?php include 'ModalProdutos.php'; ?>

