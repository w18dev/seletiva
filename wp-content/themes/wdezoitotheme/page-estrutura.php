<?php include 'header-pages.php'; ?>
	
	<div class="row col-lg-12 col-sm-12 estrutura">
		<div class="container center">
			<?php
				$post = get_post('7'); 
				$postId = $post->ID;
				$titulo = get_field('titulo_estrutura', $postId);
				$subtitulo = get_field('subtitulo_estrutura', $postId);
				$conteudo = get_field('conteudo_estrutura', $postId);
			?>
			<div class="container mobile-space">
				<div class="estrutura-title montserrat_bold">
					<h1><?php echo $titulo; ?></h1>
				</div>
				<div class="estrutura-subtitle montserrat_bold">
					<h2><?php echo $subtitulo; ?></h2>
				</div>
				<div class="estrutura-content gotham_regular">
					<?php echo $conteudo; ?>
				</div>
			</div>
			<div class="col-lg-12 col-sm-12 estrutura-posts--align">
				<div class="entry-content">
				    <?php 
				    $args = array(
				        'post_type' => 'nossos_equipamentos',
				        'post_status' => 'publish',
				        'order' => 'ASC',
				    );
				    $my_posts = new WP_Query( $args );
				    if ( $my_posts->have_posts() ) : 
				    ?>
				        <div class="my-posts">
				            <?php while ( $my_posts->have_posts() ) : $my_posts->the_post();
				            	$post 		= get_post();        
								$postId 	= $post->ID; 
								$imagem 	= get_field('imagem_do_post', $postId); 		
				             ?>
				                <div class="estrutura-posts col-lg-4 col-xs-12 col-sm-6 container left" data-est="<?php echo $postId; ?>">
									<div class="posts-img">
										<img src="<?php echo $imagem; ?>" id="estrutura-posts--img--<?php echo $postId?>">
										<div class="posts-content col-lg-12 col-xs-12">
											<div class="posts-content--text montserrat_bold text-center col-lg-8 center">
												<p id="estrutura-posts--text--<?php echo $postId; ?>"><?php the_title(); ?></p>
											</div>	
										</div>
									</div>
								</div>
				            <?php endwhile; endif; ?>
				        </div>
				</div>
			</div>
			<!-- <div class="row col-lg-12 ">
				<div class="row estrutura-plus">
					<div class="estrutura-plus--link text-center montserrat_bold" id="load-default--est">
						<a href="#" id="estrutura-plus--est">Carregar mais...</a>
					</div>
					<div id="page-loading--est" style="display: none;" class="text-center">
						<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
					</div>
				</div>
			</div> -->
		</div>
	</div>



<?php include 'fale-conosco.php'; ?>

<?php include 'contato.php'; ?>

<?php include 'footer.php'; ?>

<?php include 'ModalEstrutura.php'; ?>

