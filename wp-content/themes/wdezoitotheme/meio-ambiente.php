	<div class="row meio-amb">
		<div class="col-lg-12">
			<?php
				$post = get_post('11'); 
				$postId = $post->ID;
				$titulo = get_field('titulo_meio_ambiente', $postId);
				$conteudo = get_field('conteudo_meio_ambiente', $postId);
			?>
			<div class="container center text-center">
				<div class="meio-amb--title  montserrat_bold">
					<h1><?php echo $titulo; ?></h1>
				</div>
				<div class="meio-amb--content col-lg-10 col-xs-12 center">
					<?php echo $conteudo; ?>
				</div>
				<div class="row mobile-space container center col-lg-10 col-xs-12">
					<div class="meio-amb--circle meio-amb--img  text-center">
						<nav>
							<ul>
								<?php
									$post = get_post('11'); 
									// check if the repeater field has rows of data
									 	// loop through the rows of data
									while ( have_rows('imagens_circle') ) : the_row();
										$imagem = get_sub_field('imagem', $postId);
									        // display a sub field value 
								?>
									<li><img src="<?php echo $imagem; ?>"></li>	
									
								<?php endwhile; ?>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>