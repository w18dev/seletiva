<?php include 'header-pages.php'; ?>

	<div class="row col-lg-12 col-xs-12 servicos">
		<?php
			$post = get_post('13'); 
			$postId = $post->ID;
			$titulo = get_field('titulo_servicos', $postId);
			$conteudo = get_field('conteudo_servicos', $postId);

		?>
		<div class="container center">
			<div class="servicos-title montserrat_bold">
				<h1><?php echo $titulo; ?></h1>
			</div>
			<div class="servicos-content opensans_light">
				<p><?php echo $conteudo; ?></p>
			</div>
		</div>
			<div class="servicos-list">
				<?php
					$post = get_post('13'); 
					while ( have_rows('servicos_posts') ) : the_row();
						$imagem_post = get_sub_field('post_imagem', $postId);
						$post_titulo = get_sub_field('post_titulo', $postId);
						$post_conteudo = get_sub_field('post_conteudo', $postId);
				?>
				<div class="row servicos-container">
					<div class="servicos-container--img col-lg-6 col-xs-12 left">
						<img src="<?php echo $imagem_post; ?>">
					</div>
					<div class="servicos-container--posts col-lg-6 col-xs-12 left">
						<div class="servicos-container--posts-align">
							<div class="col-lg-6">
								<div class="posts-serv--title montserrat_bold">
									<h1><?php echo $post_titulo; ?></h1>
								</div>
								<div class="posts-serv--content opensans_light">
									<p><?php echo $post_conteudo; ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
	</div>

<?php include 'fale-conosco.php'; ?>

<?php include 'contato.php'; ?>

<?php include 'footer.php'; ?>