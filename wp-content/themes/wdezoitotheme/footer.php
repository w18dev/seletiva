        <div class="row footer">
            <div class="container center footer-aling">
                <div class="col-lg-3 col-xs-12 col-sm-12 container mobile-space">
                    <div class="footer-logo container">
                        <a href=""><img src="<?php bloginfo('template_directory'); ?>/images/logo-footer.png"></a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12 col-sm-6 col-md-6 container footer-list--border">
                    <div class="footer-list container col-xs-11 col-sm-10 col-md-10 col-lg-12">
                        <div class="footer-list--title montserrat_bold">
                            <h1>A SELETIVA</h1>
                        </div>
                        <nav class="opensans_light">
                            <ul>
                                <div class="left">
                                    <li><a href="<?php bloginfo('url'); ?>/institucional">INSTITUCIONAL</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/servicos">SERVIÇOS </a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/produtos">PRODUTOS</a></li>
                                    <li><a href="http://sagierp.com.br/integra/portal_sagi/index.php?cnpj=" target="_blank">PORTAL DO FORNECEDOR</a></li>
                                </div>
                                <div class="right">
                                    <li><a href="<?php bloginfo('url'); ?>/estrutura">ESTRUTURA</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/meio-ambiente">MEIO AMBIENTE</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/contato">CONTATO</a></li>
                                </div>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12 col-md-6 col-sm-6 mobile-space-social">
                    <div class="container">
                        <div class="footer-social container">
                            <div class="footer-social--title montserrat_bold">
                                <h1>SIGA A SELETIVA</h1>
                            </div>
                            <div class="footer-social--link montserrat_regular">
                                <a href=""><i class="fa fa-facebook-official"></i><p>FACEBOOK.COM/SELETIVA</p></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12 container mobile-space">
                    <div class="container">
                        <div class="footer-top container">
                            <div class="footer-top--link montserrat_regular">
                                <a href="#topo" id="navTop">Voltar ao topo <i class="fa fa-arrow-up"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer-black">
            <div class="container center">
                <div class="col-lg-6 col-xs-12">
                    <div class="footer-black--copiryght left montserrat_regular">
                        <p>TODOS OS DIREITOS RESERVADOS - SELETIVA 2016 ®</p>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="footer-black--developer right col-xs-6 col-lg-3 col-sm-2 col-md-2">
                        <div class="col-lg-4 col-xs-3 left">
                            <p>Feito por</p>
                        </div>
                        <div class="col-lg-8 col-xs-8 right logo-w18"> 
                            <a href="http://www.wdezoito.com.br/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-developer.png"></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.3.min.js"><\/script>')</script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/champs.min.js"></script>
        <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
               // $('#weather').rw_weather();
            });
        </script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. 
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>-->
        <script type="text/javascript">
        var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
        var page = 2;
        jQuery(function($) {
            // $("#produtos-plus").on('click', function(e){
            //     e.preventDefault();
            //     var pData = {
            //         'action': 'load_posts_by_ajax_produtos',
            //         'page': page,
            //         'security': '<?php echo wp_create_nonce("load_more_posts"); ?>'
            //     };
            //     $.ajax({
            //         type: "post",
            //         url: ajaxurl,
            //         data: pData,
            //         beforeSend: function(){
            //             document.getElementById("page-loading").style.display = "block";
            //             document.getElementById("load-default").style.display = "none";
            //         },
            //         success: function(response){
            //             document.getElementById("load-default").style.display = "block";
            //             document.getElementById("page-loading").style.display = "none";
            //             $('.my-posts').append(response);
            //             page++;
            //             ProdutosImagensUx();                        
            //         }
            //     });
            // });
            // $("#estrutura-plus--est").on('click', function(e){
            //     e.preventDefault();
            //     var pData = {
            //         'action': 'load_posts_by_ajax',
            //         'page': page,
            //         'security': '<?php echo wp_create_nonce("load_more_posts"); ?>'
            //     };
            //     $.ajax({
            //         type: "post",
            //         url: ajaxurl,
            //         data: pData,
            //         beforeSend: function(){
            //             document.getElementById("page-loading--est").style.display = "block";
            //             document.getElementById("load-default--est").style.display = "none";
            //         },
            //         success: function(response){
            //             document.getElementById("load-default--est").style.display = "block";
            //             document.getElementById("page-loading--est").style.display = "none";
            //             $('.my-posts').append(response);
            //             page++;                        
            //         }
            //     });
            // });
        });
        </script>
    </body>
</html>