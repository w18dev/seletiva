<?php include 'header-pages.php'; ?>

	<div class="row col-lg-12 institucional">
		<?php
			$post = get_post('17'); 
			$postId = $post->ID;
			$titulo = get_field('titulo_institucional', $postId);
			$conteudo = get_field('conteudo_institucional', $postId);
			$banner1 = get_field('banner_1_institucional', $postId);
			$banner2 = get_field('banner_2_institucional', $postId);

		?>
			<div class="container center">
				<div class="institucional-title montserrat_bold">
					<h1><?php echo $titulo; ?></h1>
				</div>
				<div class="institucional-content montserrat_bold gotham_regular">
					<p><?php echo $conteudo; ?></p>
				</div>
				<div class="row instituciona--link montserrat_regular">
					<a href=" <?php bloginfo('template_directory'); ?>/contato">FALE CONOSCO</a>
				</div>
			</div>
			<div class="row col-lg-12 institucional-container">
				<div class="col-lg-6 left">
					<div class="institucional-container--img">
						<img src="<?php echo $banner1; ?>">
					</div>
				</div>
				<div class="col-lg-6 left">
					<div class="institucional-container--img">
						<img src="<?php echo $banner2; ?>">
					</div>
				</div>
			</div>
	</div>

	
<?php include 'nossos-servicos.php'; ?>

<?php include 'fale-conosco.php'; ?>

<?php include 'contato.php'; ?>

<?php include 'footer.php'; ?>
