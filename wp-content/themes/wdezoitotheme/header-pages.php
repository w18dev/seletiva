<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> 
<!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/champs.min.css">

        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/modernizr-2.6.2.min.js"></script>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    </head>
    <body>
        <div class="row col-lg-12 col-xs-12 col-md-12 col-sm-12">
            <a name="topo"></a>
            <div class="row header pages">
                <div class="container center header-container col-lg-12 col-xs-12">
                    <div class="left header-logo col-lg-3 col-xs-6 left container">
                        <div class="container logo-default">
                            <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo-pages.png"></a>
                        </div>
                        <div class="container logo-scroll">
                            <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo-header.png"></a>
                        </div>
                    </div>
                    <div class="row montserrat_regular mobile-hide tablet-hide">
                        <div class="text-right header-list col-lg-6 left">
                            <nav>
                                <ul>
                                    <li><a href="<?php bloginfo('url'); ?>/institucional" id="institucional">INSTITUCIONAL</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/servicos" id="servicos">SERVIÇOS </a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/produtos" id="produtos">PRODUTOS</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/estrutura" id="estrutura">ESTRUTURA</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/contato" id="contato">CONTATO</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="link-portal col-lg-3 left">
                            <a href="http://sagierp.com.br/integra/portal_sagi/index.php?cnpj=" target="_blank">PORTAL DO FORNECEDOR</a>
                        </div>
                    </div>
                    <div class="mobile-menu mobile-show tablet-show col-xs-2 right">
                        <header class="menu">
                            <div class="icon-menu pages" id="open-menu">
                                <s class="bar"></s>
                                <s class="bar"></s>
                                <s class="bar"></s>
                            </div>
                        </header>
                        <aside class="sidebar" id="close-menu">
                            <div class="icon-close text-right">
                                <i class="fa fa-times" id="icon-close"></i>
                            </div>
                            <nav class="nav-menu  montserrat_regular text-right">
                                <ul>
                                    <li><a href="<?php bloginfo('url'); ?>/institucional" id="institucional">INSTITUCIONAL</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/servicos" id="servicos">SERVIÇOS </a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/produtos" id="produtos">PRODUTOS</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/estrutura" id="estrutura">ESTRUTURA</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/contato" id="contato">CONTATO</a></li>
                                    <li><a href="http://sagierp.com.br/integra/portal_sagi/index.php?cnpj=" target="_blank">PORTAL DO FORNECEDOR</a></li>
                                </ul>
                            </nav>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        <?php if(is_page('institucional')): ?>
            <script type="text/javascript">
                document.getElementById("institucional").setAttribute("class","active");
            </script>
        <?php else: ?>
            <?php if(is_page('servicos')): ?>
                <script>
                    document.getElementById("servicos").setAttribute("class","active");
                </script>
            <?php else: ?>
                <?php if(is_page('produtos')): ?>
                    <script>
                        document.getElementById("produtos").setAttribute("class","active");
                    </script>
                    <?php else: ?>
                        <?php if(is_page('estrutura')): ?>
                            <script>
                                document.getElementById("estrutura").setAttribute("class","active");
                            </script>
                            <?php else: ?>
                                <?php if(is_page('meio-ambiente')): ?>
                                    <script>
                                        document.getElementById("ambiente").setAttribute("class","active");
                                    </script>
                                <?php else: ?>
                                    <?php if(is_page('contato')): ?>
                                        <script>
                                            document.getElementById("contato").setAttribute("class","active");
                                        </script>
                                    <?php endif; ?>
                        <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>