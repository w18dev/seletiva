	<div class="row contato">
		<div class="container center">
			<div class="col-lg-12 col-xs-12 col-sm-12">
				<?php
			        $post = get_post('9'); 
			        $postId = $post->ID;
			        $titulo_contato = get_field('titulo', $postId);
			        $conteudo_contato = get_field('conteudo', $postId);
			    ?>
				<div class="contato-block--left col-lg-3 col-xs-12 col-sm-12 left">
					<div class="block-left--title montserrat_bold">
						<h1><?php echo $titulo_contato; ?></h1>
					</div>
					<div class="block-left--content opensans_light">
						<p><?php echo $conteudo_contato; ?></p>
					</div>
				</div>
				<div class="col-lg-8 col-xs-12 col-sm-12 contato-form right">
					<div class="montserrat_regular container">
						<form>
							<div class="row form-align">
								<div class="col-lg-8 col-xs-12 col-sm-12 container form-field">
									<p>Nome</p>
									<input type="text" id="txtNome" name="contato[nome]" value="" required/>
								</div>
								<div class="col-lg-4 col-xs-12 col-sm-12  container form-field">
									<p>E-mail</p>
									<input type="email" id="txtEmail" name="contato[email]" value="" class="form-requerid"/>
								</div>
							</div>
							<div  class="row form-align">
								<div class="col-lg-5 col-xs-12 col-sm-12  container form-field">
									<p>Telefone</p>
									<input type="text" id="txtTelefone" name="contato[telefone]" value="" data-mask="celular" class="form-mask-js" required/>
								</div>
								<div class="col-lg-3 col-xs-12 col-sm-12 container form-field">
									<p>Empresa</p>
									<input type="text" id="txtEmpresa" name="contato[empresa]" value="" />
								</div>
								<div class="col-lg-4 col-xs-12 col-sm-12  container form-field">
									<p>Cargo / Função</p>
									<input type="text" id="txtCargo" name="contato[cargo]" value="" />
								</div>
							</div>
							<div class="row col-lg-12 col-xs-12 col-sm-12 container form-field mobile-space">
								<p>escreva aqui sua mensagem</p>
								<textarea name="contato[mensagem]" required></textarea>
							</div>
							<div class="row container button-contato">
								<div class="form-field  montserrat_bold right ">
									<button type="submit" name="enviar" id="btnEnviar">ENVIAR</button>
								</div>
							</div>
						</form>
						<div id="EnvioOk">
							<p>Dados enviados com sucesso !!</p>
						</div>
						<div id="EnvioError">
							<p>Por favor, preencha todos os campos !</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
