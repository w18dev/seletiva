$(document).ready(function(){
	ProdutosImagensUx();
	EstruturaImagensUx();
	ContainerHeight();
	/*
	////////////////////////////////
		Raro! Boilerplate JS
	///////////////////////////////
	*/

		//Enter your code here
		
	/*
		#############################
		 RETORNAR AO TOPO SUAVEMENTE
		#############################
	*/
		$("#navTop").click(function(){
			$('body').animate({
				scrollTop: 0
			}, 1000)
		});
	/*
		###############################
		 //RETORNAR AO TOPO SUAVEMENTE
		###############################
	*/


	/*
		#######################
		 	MENU RESPONSIVE
		#######################
	*/
		$('#open-menu').click(function(){
			if($(this).hasClass('mov')){
				$(this).removeClass('mov');
				$('#close-menu').css({'right' : '-100%'});
			}else{
				$(this).addClass('mov');
				$('#close-menu').css({'right' : '0'});
			}
		});

		$("#icon-close").click(function(){
			$('#close-menu').css({'right' : '-100%'});
		});

		$(window).scroll(function(){
			$('#close-menu').css({'right' : '-100%'});
		});
	/*
		#######################
		   //MENU RESPONSIVE
		#######################
	*/


	/*
		#######################
			MENU FIXO SCROOL
		#######################
	*/
		$(window).scroll(function(){
			var top = ($(window).scrollTop());
			if(top >= 120){
				$('.header').addClass('header-scroll');
				$('.logo-scroll').css({'display':'block'});
				$('.logo-default').css({'display':'none'});
			}
			else
			{
				$('.logo-default').css({'display':'block'});
				$('.logo-scroll').css({'display':'none'});
				$('.header').removeClass('header-scroll');
			}
		});

	/*
		#######################
			MENU FIXO SCROOL
		#######################
	*/

});

function ProdutosImagensUx(){
	$('.produtos-posts').on('click', function(){
		var idPost = $(this).attr('data-post');
		var img = $("#produtos-posts--img--" + idPost).attr('src');
		var text = $("#produtos-posts--text--" + idPost).text();

		$("#modalImg").attr('src', img);
		$("#modalText").text(text);

		console.log(idPost);
		console.log(img);
		console.log(text);

		var modal = new Modal(),
		clone = $("#ModalProdutos").clone();
		modal.setContainer(clone[0]);
		modal.setClassModalContainer("open");
		modal.setClassModalContainer("center");
		modal.showModal();

	});
}

function EstruturaImagensUx(){
	$('.estrutura-posts').on('click', function(){
		var idPostEst = $(this).attr('data-est');
		var imgEst = $("#estrutura-posts--img--" + idPostEst).attr('src');
		var textEst = $("#estrutura-posts--text--" + idPostEst).text();

		$("#modalImgEst").attr('src', imgEst);
		$("#modalTextEst").text(textEst);

		console.log(idPostEst);
		console.log(imgEst);
		console.log(textEst);

		var modalEst = new Modal(),
		cloneEst = $("#ModalEstrutura").clone();
		modalEst.setContainer(cloneEst[0]);
		modalEst.setClassModalContainer("open");
		modalEst.setClassModalContainer("center");
		modalEst.showModal();
	});
}


function ContainerHeight(){
	var container = document.getElementsByClassName("article-post");
	var size = 0;
	for(var i = 0; i < container.length; i++){
		var post = container[i],
		sizePost = post.clientHeight;
		if(size == 0){
			size = sizePost;
		}
		else{
			if(sizePost > size){
				size = sizePost;
			}
		}
	}
	for(var i = 0; i < container.length; i++){
		var post = container[i],
		sizePost = post.clientHeight;
		post.style.height = size + "px";
	}	
}
