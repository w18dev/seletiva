/*
##################################
	ANCHOR
##################################
*/
	function anchorScroll(data){
		var device 	= getDevice(),
			nav 	= 100;
		switch(data){
			case "#":
				$("html, body").animate({ scrollTop: 0 }, 1000); 
				break;
			default:
				$("html, body").animate({ scrollTop: $(data).offset().top - nav }, 1000);
				break;
		}
	}
/*
##################################
	//ANCHOR
##################################
*/
/*
##################################
	NAVSCROLLING
##################################
*/
	function navScrolling(){
		var win_height = $(window).height()/2;
		$(window).on('scroll', function(){
			if($(window).scrollTop() >= win_height){
				$('.nav.nav-fixed').addClass("nav-fixed-scrolling");					
			}else{
				$('.nav.nav-fixed').removeClass("nav-fixed-scrolling");					
			}
		});
	}
/*
##################################
	//NAVSCROLLING
##################################
*/
/*
##################################
	GET DEVICE
##################################
*/
	function getDevice(){
		var device_width = $(window).width(),
			result = "desktop";
		if(device_width >= 1200 && device_width <= 1440){
			result = "notebook";
		}else{
			if(device_width > 992 && device_width < 1200){
				result = "netbook";
			}else{
				if(device_width >= 641 && device_width < 992){
					result = "tablet";
				}else{
					if(device_width >= 320 && device_width < 640){
						result = "mobile";
					}
				}				
			}
		}
		return result;
	}
/*
##################################
	//GET DEVICE
##################################
*/

/*
##################################
	FORM
##################################
*/
	/*
	==================================
		CHECK REQUERID
	==================================
	*/
		function formCheckRequerid(){
			var requerids = document.getElementsByClassName("form-requerid"),
				status = true;
			formFieldClear();
			if(requerids.length > 0){
				for(var i = 0; i < requerids.length; i++){
					if(requerids[i].value == ""){
						status = false;
						formFieldError(requerids[i]);
					}
				}
			}else{
				status = false;
			}
			return status;
		}
	/*
	==================================
		//CHECK REQUERID
	==================================
	*/
	/*
	==================================
		CHECK EMAIL
	==================================
	*/
		function formCheckEmail(data){
			var field = document.getElementById(data),
				value = field.value,
				status = true;
			if(value.indexOf("@") > 0){
				var arroba = value.indexOf("@");
				if(value.indexOf(".") > arroba + 1){	
				}else{
					status = false;
					formFieldError(field);
				}
			}else{
				status = false;
				formFieldError(field);
			}
			return status;
		}
	/*
	==================================
		//CHECK EMAIL
	==================================
	*/
	/*
	==================================
		FIELD
	==================================
	*/
		/*
		---------------------------------
			ERROR
		---------------------------------
		*/
			function formFieldError(field){
				var parent = field.parentNode;
				parent.classList.add("has-error");
			}
		/*
		---------------------------------
			//ERROR
		---------------------------------
		*/
		/*
		---------------------------------
			ERROR
		---------------------------------
		*/
			function formFieldClear(){
				var field = document.getElementsByClassName('form-field');
				for(var i = 0; i < field.length; i++){
					field[i].classList.remove("has-error");
					field[i].classList.remove("has-success");
					field[i].classList.remove("has-warning");
				}
			}
		/*
		---------------------------------
			//ERROR
		---------------------------------
		*/
	/*
	==================================
		//FIELD
	==================================
	*/
/*
##################################
	//FORM
##################################
*/