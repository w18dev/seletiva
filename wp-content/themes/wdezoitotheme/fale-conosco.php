	<div class="row fale-conosco">
		<div class="container center">
			<div class="fale-conosco--title  montserrat_regular text-center">
				<h1>fale conosco</h1>
			</div>
			<div class="col-lg-12 col-xs-12 col-sm-12 fale-conosco--container">
				<?php 
		    		$query = "post_type=fale_conosco&post_status=publish&showposts=-1";
		    		query_posts($query);

		    		while(have_posts()):
		    			the_post();
		    			$post = get_post();        
		    			$postId = $post->ID;   		  		
		    			$endereco = get_field(removeParagraph('endereco', $postId));   		
		    			$numero = get_field(removeParagraph('numero', $postId));   		
		    			$bairro = get_field(removeParagraph('bairro', $postId));   		
		    			$e_mail = get_field(removeParagraph('e_mail', $postId));   		
		    			$telefone = get_field(removeParagraph('telefone', $postId));   		

		    	?>
					<div class="fale-conosco--locais col-lg-3 col-sm-6 col-xs-12 col-md-3 text-center left container">
						<div class="locais-icon">
							<i class="fa fa-flag"></i>
						</div>
						<div class="locais-city montserrat_bold">
							<h1><?php the_title(); ?></h1>
						</div>
						<div class="locais-content montserrant_regular col-lg-10 center">
							<p>
								<?php echo $endereco; ?>
								<?php echo $numero; ?><br/>
								<?php echo $bairro; ?><br/>
								<?php echo $e_mail; ?><br/>
								<span><?php echo $telefone; ?></span>
							</p>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>