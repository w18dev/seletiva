	<div class="row">
		<div class="container center article">
			<div class="row co-lg-12 col-xs-12 col-sm-12">
				<div class="article-title montserrat_bold">
					<h1>conheça nossos SERVIÇOS</h1>
				</div>
			</div>
			<div class="row col-lg-12 col-xs-12 col-sm-12 col-md-12 article-container">
				<?php 
            		$query = "post_type=nossos_servicos&post_status=publish&showposts=-1";
            		query_posts($query);

            		while(have_posts()):
            			the_post();
            			$post = get_post();        
            			$postId = $post->ID;
            			$imagem = get_field('imagem_servico', $postId);   		  		
            			$conteudo_servico = get_field('conteudo_servico', $postId);   		

            	?>
					<div class="article-post col-lg-3 col-sm-5 col-xs-12 col-md-6 left">
						<div class="article-post--img">
							<img src="<?php echo $imagem; ?>">
						</div>
						<div class="article-post--title montserrat_bold col-sm-10 col-md-7">
							<h1><?php the_title(); ?></h1>
						</div>
						<div class="article-post--content opensans_light col-sm-10 col-md-7">
							<p><?php echo $conteudo_servico; ?></p>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
			<div class="row col-lg-12 col-xs-12">
				<div class="center col-lg-6">
					<div class="article-plus montserrat_bold text-center text-center">
						<a href="<?php bloginfo('template_directory')?>/servicos">SAIBA MAIS SOBRE NOSSOS SERVIÇOS</a>
					</div>
				</div>
			</div>
		</div>
	</div>