<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'w18_seletiva');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'frasW5xaBRes');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wpL`<:-slcEQ}7(K:!/ph/_PA42((Pl$PB8WAH:!fm`vO612i_^h5OLRaM.LSq08');
define('SECURE_AUTH_KEY',  'A|VPR]_8ggmuD#lnR#Kc^f%0)Z0ZeC~ /L4*M} ((vI~c3.F`uP|U)=3+NgN8ov`');
define('LOGGED_IN_KEY',    'hd|GBe![%$hM{^}l((x4[;qViSyRDmrZ$nDr;wN& |d<o>19[V,FIC&SnFr5T-;/');
define('NONCE_KEY',        '@7i^4jju&,N-PQwYS<OIisy^.1o9R3ZQky!,brZakk:~0sH=4L|8rZVeU[[E!9ze');
define('AUTH_SALT',        '{/qia3^JTnN@X@]-|BLyIacyS0cb:kQ.EYO|@lIKpHRl?sbH4CdV>e2] 8k]MV#i');
define('SECURE_AUTH_SALT', 'uM,W}Ny~aCMdR@4wlaV^k;f))*Bj4&y@^_Jl1(kz#lgTp-A|*:_9r_8~/&5+bG~:');
define('LOGGED_IN_SALT',   '_Y_C):mz|6A!6C718svZ~c;[n`07dz5$ 59D!]dYf5ftVO/;5GAU]Kq%-EPY$}wW');
define('NONCE_SALT',       'JiI(YD*o1pT-ylXZ?T*Y;`pnN>{v<R2{;~>j]|0b}5y;z-ccL<gR0,z-eek(<%]b');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
